# Shock-bubble interaction problem

This data set contains a simulation of the shock-bubble interaction problem.
The data set was produced using [dune-fem-dg](https://gitlab.dune-project.org/dune-fem/dune-fem-dg).


## Data description

The data contains the conservative variables

```math
 U = (\rho, u_x, u_y, u_z, E) = (solution_0,...,solution_4).
```


Primitive variables like pressure and velocity or temperature have to be
computed in ParaView with the calculator. The corresponding formulas are

$$ p = 0.4 () $$.
