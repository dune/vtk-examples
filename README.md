# vtk-examples

The following data sets have been created with the [dune-fem tutorial](https://dune-project.org/sphinx/content/sphinx/dune-fem/).

## Data set description

### Crystal growth

The folder **crystal** contains a 2d adaptive simulation for a simple crystal
growth problem. A more detailed description is found [here](https://dune-project.org/sphinx/content/sphinx/dune-fem/crystal_nb.html#index-0).

### Mean curvature flow

The folder **meancurvature** contains a 2d surface problem embedded in 3d
stemming from a specific example of a geometric evolution equation where
the evolution is governed by the mean curvature. A more detailed description is
found [here](https://dune-project.org/sphinx/content/sphinx/dune-fem/mcf_nb.html#index-0).

### Shock-bubble interaction problem

The folder **euler-3d** contains a simulation of the shock-bubble interaction problem.
The data set was produced using [dune-fem-dg](https://gitlab.dune-project.org/dune-fem/dune-fem-dg).

#### Data description

The data contains the conservative variables density $`\rho`$, momentum
$`u = (u_x,u_y,u_z)^T`$ and total energy $`E`$.

```math
U = (\rho, u_x, u_y, u_z, E)^T = (solution_0,...,solution_4)^T.
```

Primitive variables like pressure or velocity have to be
computed in ParaView with the calculator. The corresponding formulas are
for velocity $`v`$:
```math
v = (v_x, v_y, v_z)^T = \frac{1}{\rho}(u_x, u_y, u_z)^T
```

for pressure $`p`$:
```math
p = \frac{2}{5} (E - \frac{1}{2} (v \cdot u )) .
```
